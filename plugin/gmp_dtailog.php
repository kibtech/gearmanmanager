<?php
/*
 * 示例worker
 * 
 * 参数规则：
 * 传过来的参数是json格式结构，需要json解码
 * 由于参数按照json字符串传输，参数的数据结构对gearman透明，
 * 调用端与worker端必须明确json解码后参数结构的意义
 * 不再需要由worker来存储结果，gearman管理机制能捕捉到worker的返回值，按固定统一格式存储
 * 因为php本身的普通函数是不能重定义，重加载，或者是删除函数定义的
 * 所以worker函数写成闭包语法，在gearman管理的时候，能容易地动态修改worker函数
 */
  /**
   * @param integer $lineno  返回行数
   * @param string $level reduce|map 这个worker是一个map|reduce，两重功能
   * @param string $regex  过滤规则，grep语法
   */
$_function = function ($job) use (&$g_workerman)   /* 可选，把全局变量带进worker函数，可以直接使用 */ 
{
    $json_args = $job->workload();
    $args = json_decode($json_args, true);
    print_r($args);

    $lineno = isset($args['lineno']) ? $args['lineno'] : 10;
    $level = isset($args['level']) ? $args['level'] : 'reduce';
    $regex = isset($args['regex']) ? $args['regex'] : null;

    $result = array();
    $cb_data = function ($task) use (&$result, &$job) {
        $jretdata = $task->data();
        $retdata = json_decode($jretdata, true);
        if (!isset($result['data'])) {
            $result['data'] = $jretdata;
        } else {
            $result['data'] .= $jretdata;
        }

        $job->sendData($jretdata);
    };

    if ($level == 'reduce') {
        $jservs = $g_workerman->servers();
        $gmc = new GearmanClient();
        $gmc->setDataCallback($cb_data);
        $gmc->addServers($jservs[rand()%(count($jservs))]);

        $jnodes = $gmc->doNormal('get_nodes', json_encode(array('v')));

        print_r($jnodes);

        $nodes = json_decode($jnodes); // array('host:port' => pid, ...)
        
        $task_objs = array();
        foreach ($nodes as $hp => $pid) {
            $func_name = 'gmwn_' . explode(':', $hp)[0] . '_dtailog' ;
            $func_args = json_encode(array('level'=>'map', 'lineno' => $lineno, 'regex' => $regex));
            $task_objs[] = $gmc->addTask($func_name, $func_args);
        }

        $res = $gmc->runTasks();
        $result['result'] = $res;
        foreach ($task_objs as $task_obj) {
            $retdata = $task_obj->data();
            // var_dump($retdata);
            // $result[] = $retdata;
        }

        // $result['data'] = json_decoe($result['data']);
        // var_dump($result);
        // $result = $res . $jnodes;
    } else if ($level == 'map') {
        $log_dir = $g_workerman->getLogDir();
        $tail_cmd = "tail -n {$lineno} {$log_dir}/gmworker.*.log 2>&1";
        if (!empty($regex)) {
            $tail_cmd .= " | grep \"{$regex}\"";
        }
        echo "herehehere, {$tail_cmd} \n";
        $last_line = exec($tail_cmd, $output, $retval);
        // $raw_output = implode("\n", $output);

        $str_output = json_encode($output, JSON_UNESCAPED_UNICODE);

        $job->sendData($str_output);

        // for test, incremental send data test
        for ($i = 0; $i < 20; $i++) {
            sleep(1);
            $job->sendData("haha done le: {$i}\n");
        }

        $job->sendComplete('222');
        $job->complete('111');
        $job->setReturn(GEARMAN_SUCCESS);
        $job->setStatus(100, 100);


        $result = 'line count:' . count($output);
    }

    // $result = array('dummy', 'dummy123' . rand(), $args);
    // 演示结果返回
    $json_result = json_encode($result, JSON_UNESCAPED_UNICODE);
    return $json_result;
};

///////////////////////////.////////////////////
// $_function = null;
$_register_name = 'dtailog';
$_enable = true;
return array($_function, $_register_name, $_enable);
// array('func_obj', 'reg_name', 'enable');