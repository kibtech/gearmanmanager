<?php
/*
 *
 */

  /**
   * FastCGIClient的闭包版本，可动态切换更新
   *
   * Usage:
   *      $client = $FastCGIClientFunctor;
   *      $res = $client->__invoke($params, $stdin, $host, $port);
   *      print_r($res);
   */

$FastCGIClientFunctor = function (array $params, $stdin, $host = '127.0.0.1', $port = 9000)
{
    if (!class_exists('FastCGIClientImpl')) {
        require(__DIR__ . '/../class/fcgic.php');
    }

    // funtor() 调用的实现
    $_impl = new FastCGIClientImpl();
    if (!$_impl->connect($host, $port)) {
        echo "Can not connect to host, {$host}:{$port}.\n";
        return false;
    }

    $_impl->request($params, $stdin);
    // $_impl->requestFullTest($params, $stdin);
    $bv = $_impl->parseHttpHeader($_impl->_stdout_raw_content);

    $res = array('code' => $_impl->_http_status_code,
                 'status' => $_impl->_http_status_msg,
                 'stdout' => $_impl->_stdout_real_content,
                 'stderr' => $_impl->_stderr_content);

    $_impl->disconnect();
    $_impl = null;

    return $res;
};

/*
$client = $FastCGIClient_functor;
$content = 'key123=value456&keyabc=valueefggg&中文=abcdefg&hehe=汉字utf8的';
$res = $client->__invoke(
                      array(
                            'GATEWAY_INTERFACE' => 'FastCGI/1.0',
                            'REQUEST_METHOD' => 'POST',
                            'DOCUMENT_ROOT' => '/data1/vhosts/photo.house.kitech.com.cn',
                            'SCRIPT_FILENAME' => '/data1/vhosts/photo.house.kitech.com.cn/index.php',
                            'SCRIPT_NAME' => '/index.php',
                            'REQUEST_URI' => '/test/test6/simpost',
                            'SERVER_SOFTWARE' => 'php/fastcgiclient',
                            'REMOTE_ADDR' => '127.0.0.1',
                            'REMOTE_PORT' => '9985',
                            'SERVER_ADDR' => '127.0.0.1',
                            'SERVER_PORT' => '80',
                            'SERVER_NAME' => 'photo.house.kitech.com.cn',
                            'HTTP_HOST' => 'photo.house.kitech.com.cn',
                            'SERVER_PROTOCOL' => 'HTTP/1.0',
                            'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                            'CONTENT_LENGTH' => strlen($content),
                            'kitech.com.cn_CACHE_DIR' => '',
                            'kitech.com.cn_DATA_DIR' => '',
                            'kitech.com.cn_RSYNC_SERVER' => '',
                            'kitech.com.cn_STORAGE_SERVER' => '',
                            'kitech.com.cn_RSYNC_MODULES' => '',
                            'kitech.com.cn_RESOURCE_URL' => '',
                            'kitech.com.cn_DIST_URL' => '',
                            'kitech.com.cn_TAAA_127' => 'DallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDall123456789012345',
                            'kitech.com.cn_TAAA_128' => 'DallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDallasDall1234567890123456',
                                   ),
                      $content
                      );


*/

$_function = function ($job) use (&$g_workerman, &$FastCGIClientFunctor)   /* 可选，把全局变量带进worker函数，可以直接使用 */ 
{
    $json_args = $job->workload();
    $args = json_decode($json_args, true);
    print_r($args);
    if (is_string($args['_SERVER'])) {
        // 解码服务器变量配置信息
        $args['_enc_SERVER'] = $args['_SERVER'];
        $args['_SERVER'] = json_decode(gzuncompress(base64_decode($args['_SERVER'])), true);
        print_r($args);
    }
    

    $result = array('dummyddd1234567109801234567890123456789012345', 'dummy123456' . rand(), $args);

    $fcgi_params = array();
    $fcgi_stdin = '';
    $fcgi_host = '127.0.0.1';
    $fcgi_port = '9000';

    // request k/vs
    if (isset($args['params'])) {
        foreach ($args['params'] as $key => $value) {
            $fcgi_stdin .= "{$key}=" . urlencode($value) . '&';
        }
    }

    // 协议级常量
    $proto_const_params = array(
                                'GATEWAY_INTERFACE' => 'FastCGI/1.0',
                                'REMOTE_ADDR' => '',  // 这个是必须的
                                'SERVER_SOFTWARE' => 'gmfcgi/1.0',
                                'SERVER_PROTOCOL'  => 'HTTP/1.0',
                                );
    // 项目级变量
    $app_must_params = array(
                             'REQUEST_METHOD' => 'POST', 
                             'SCRIPT_NAME' => '',
                             'REQUEST_URI' => '',
                             'SERVER_NAME' => '',
                             'HTTP_HOST' => '',
                             'SERVER_ADDR' => '',
                             'REQUEST_URI' => "",
                             'PATH_INFO' => '',
                             'DOCUMENT_URI' => '',
                             'SERVER_NAME' => '',
                             'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                             'CONTENT_LENGTH' =>  0,
                             'HOME' => '',
                             );

    foreach ($proto_const_params as $key => $value) {
        $fcgi_params[$key] = $value;
    }
    
    // add http app env params
    foreach ($args['_SERVER'] as $key => $value) {
        $fcgi_params[$key] = $value;
    }


    // add http request params
    $fcgi_params['REQUEST_METHOD'] = 'POST';
    $fcgi_params['REQUEST_URI'] = "/{$args['app']}/{$args['controller']}/{$args['action']}";
    $fcgi_params['PATH_INFO'] = $fcgi_params['REQUEST_URI'];
    $fcgi_params['DOCUMENT_URI'] = "/index.php";
    $fcgi_params['SCRIPT_NAME'] = $fcgi_params['DOCUMENT_URI'];
    $fcgi_params['HOME'] = $args['_SERVER']['DOCUMENT_ROOT'];
    $fcgi_params['SERVER_NAME'] = $args['domain'];
    $fcgi_params['HTTP_HOST'] = $args['domain'];
    $fcgi_params['CONTENT_TYPE'] = 'application/x-www-form-urlencoded';
    $fcgi_params['CONTENT_LENGTH'] =  strlen($fcgi_stdin);


    $fcgi_host = $args['_SERVER']['SERVER_ADDR']; // php-fpm的gearman分组需要listen在内网卡IP，而不是是127.0.0.1    
    $client = $FastCGIClientFunctor;
    $fcgi_res = $client->__invoke($fcgi_params, $fcgi_stdin, $fcgi_host, $fcgi_port);
    $result = $fcgi_res;


    // 演示结果返回
    $json_result = json_encode($result, JSON_UNESCAPED_UNICODE);
    return $json_result;
};



///////////////////////////.////////////////////
// $_function = null;
$_register_name = 'run_fastcgi_action';
$_enable = true;
return array($_function, $_register_name, $_enable);
// array('func_obj', 'reg_name', 'enable');

